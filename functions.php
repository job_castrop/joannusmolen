<?php
/**
 * Timber starter-theme
 * https://github.com/timber/starter-theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if (!class_exists('Timber')) {
    add_action('admin_notices', function () {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
    });

    add_filter('template_include', function ($template) {
        return get_stylesheet_directory() . '/static/no-timber.html';
    });

    return;
}

/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = ['templates', 'views'];

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;


/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class StarterSite extends Timber\Site
{
    /** Add timber support. */
    public function __construct()
    {
        add_action('acf/init', [$this, 'my_acf_init']);
        add_action('after_setup_theme', [$this, 'theme_supports']);
        add_filter('timber/context', [$this, 'add_to_context']);
        add_filter('timber/twig', [$this, 'add_to_twig']);
        add_action('init', [$this, 'register_post_types']);
        add_action('init', [$this, 'register_taxonomies']);
        add_action('init', [$this, 'enqueue_script']);
        add_action('init', [$this, 'enqueue_style']);
        add_action('init', [$this, 'register_block_type']);
        add_filter('show_admin_bar', [$this, 'js_hide_admin_bar']);
        add_action('admin_init', [$this, 'ace_block_wp_admin']);
        add_filter('login_redirect', [$this, 'js_login_redirect'], 10, 3);
        add_action('enqueue_block_editor_assets', [$this, 'add_gutenberg_assets']);
        add_action('comment_post', [$this, 'comment_post'], 10 ,2);
        add_action('init', [$this, 'my_add_template_to_posts']);

        add_theme_support('yoast-seo-breadcrumbs');

        if (function_exists('acf_add_options_page')) {
            acf_add_options_page();
        }

        add_action('wp_ajax_add_like', [$this, 'add_like']);
        add_action('wp_ajax_nopriv_add_like', [$this, 'add_like']);

        add_action('wp_ajax_filter_recepten', [$this, 'filter_recepten']);
        add_action('wp_ajax_nopriv_filter_recepten', [$this, 'filter_recepten']);

        parent::__construct();
    }

    public static function my_acf_init()
    {
        $context = Timber::context();
        if(isset($context['options']['google_api_key'])) {
            acf_update_setting('google_api_key', $context['options']['google_api_key']);
        }
    }

    public function comment_post($comment_ID, $comment_approved)
    {
        $stars = filter_input(INPUT_POST, 'stars');
        $comment = get_comment($comment_ID);
        update_field('stars', $stars, 'comment_' . $comment_ID);
        $stars = self::get_stars($comment->comment_post_ID);
        update_field('stars', $stars, $comment->comment_post_ID);

    }

    public static function get_stars($ID)
    {
        $stars = 0;
        $comments = get_comments($ID);
        foreach ($comments as $comment) {
            if(!$comment->comment_approved) continue;
            $fields = get_fields('comment_' . $comment->comment_ID);
            if(isset($fields['stars'])) {
                $stars += $fields['stars'];
            }
        }
        return ceil($stars / count($comments));
    }

    public function filter_recepten()
    {
        $formdata = filter_input(INPUT_POST, 'formdata');
        parse_str($formdata, $data);
        $categories = [];
        foreach ($data['filter'] as $category => $on)
        {
            $categories[] = $category;
        }
        $context = Timber::get_context();

        $args = [
            'post_type' => 'recepten',
            'posts_per_page' => -1,
            'orderby' => [
                'date' => 'DESC'
            ],
            'category' => implode(',', $categories)
        ];

        $context['recepten'] = Timber::get_posts($args);
        $return['recepten']  = Timber::fetch('partial/recepten.twig', $context);

        $return['subcats'] = '';
        if($categories) {
            $context['categories'] = Timber::get_terms('category', ['parent' => implode(',', $categories)]);
            $return['subcats']  = Timber::fetch('partial/filters.twig', $context);
        }
        echo json_encode($return);
        exit();
    }

    public function add_like()
    {
        $post_id = filter_input(INPUT_POST, 'post_id');
        $likes = get_field('likes', $post_id);
        $likes++;
        update_field('likes', $likes, $post_id);
        echo $likes;
        exit();
    }

    public function add_gutenberg_assets()
    {
        wp_enqueue_style('bootstrap-gutenberg', get_theme_file_uri('/assets/css/bootstrap.css'), false);
        wp_enqueue_style('joannusmolen-gutenberg', get_theme_file_uri('/assets/css/joannusmolen.min.css'), false);
    }

    public function register_block_type()
    {
        if (!function_exists('acf_register_block')) {
            return;
        }

        // Register a new block.
        acf_register_block([
            'name' => 'verkooppunt',
            'title' => __('Verkooppunt', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/verkooppunt-block.twig',
            'category' => 'formatting',
            'icon' => 'dashicons-store',
            'keywords' => ['example'],
            'align' => 'full',
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'video_title_text',
            'title' => __('Video, titel & tekst', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/video_title_text-block.twig',
            'category' => 'formatting',
            'icon' => 'playlist-video',
            'keywords' => ['example'],
            'align' => 'full',
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'subscribe_form',
            'title' => __('Aanmeldformulier', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_subscribe_form'],
            'category' => 'formatting',
            'icon' => 'feedback',
            'keywords' => ['example'],
            'align' => 'full'
        ]);

        acf_register_block([
            'name' => 'keurmerken',
            'title' => __('Keurmerken', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/keurmerken.twig',
            'category' => 'formatting',
            'icon' => 'awards',
            'keywords' => ['example'],
            'align' => 'full',
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'features',
            'title' => __('Features', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/features.twig',
            'category' => 'formatting',
            'icon' => 'star-filled',
            'keywords' => ['example'],
            'align' => 'full',
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'customtitle',
            'title' => __('Titel Joannusmolen', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/custom-title.twig',
            'category' => 'formatting',
            'icon' => 'editor-aligncenter',
            'keywords' => ['example'],
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'textblock',
            'title' => __('Tekst block', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/text-block.twig',
            'category' => 'formatting',
            'icon' => 'layout',
            'keywords' => ['example'],
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'productenzoeken',
            'title' => __('Producten zoeken', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/producten-zoeken.twig',
            'category' => 'formatting',
            'icon' => 'products',
            'keywords' => ['example'],
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'verkooppuntvinden',
            'title' => __('Verkooppunt vinden', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/verkooppunt-vinden.twig',
            'category' => 'formatting',
            'icon' => 'products',
            'keywords' => ['example'],
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'faq',
            'title' => __('FAQ', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_faq'],
            'render_template' => '/block/faq.twig',
            'category' => 'formatting',
            'icon' => 'lightbulb',
            'keywords' => ['example'],
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'social',
            'title' => __('Socialmedia block', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/social-block.twig',
            'category' => 'formatting',
            'icon' => 'facebook',
            'keywords' => ['example'],
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'overgang',
            'title' => __('Overgang', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/overgang.twig',
            'category' => 'formatting',
            'icon' => 'image-flip-vertical',
            'keywords' => ['example'],
            'mode' => 'auto',
        ]);


        acf_register_block([
            'name' => 'producten',
            'title' => __('Producten', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_products_block'],
            'render_template' => '/block/producten.twig',
            'category' => 'formatting',
            'icon' => 'products',
            'keywords' => ['example'],
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'productafbeeldingen',
            'title' => __('Productafbeeldingen', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/productafbeeldingen.twig',
            'category' => 'formatting',
            'icon' => 'gallery',
            'keywords' => ['example'],
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'ingredienten',
            'title' => __('Ingrediënten en voedingswaarden', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/ingredienten_en_voedingswaarden.twig',
            'category' => 'formatting',
            'icon' => 'gallery',
            'keywords' => ['example'],
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'bereidingswijze',
            'title' => __('Ingrediënten en bereidingswijze', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/bereidingswijze.twig',
            'category' => 'formatting',
            'icon' => 'gallery',
            'keywords' => ['example'],
            'mode' => 'auto',
        ]);

        acf_register_block([
            'name' => 'contactgegevens',
            'title' => __('Contactgegevens', 'joannusmolen'),
            'description' => __('A custom example block.', 'joannusmolen'),
            'render_callback' => [$this, 'render_block'],
            'render_template' => '/block/contactgegevens.twig',
            'category' => 'formatting',
            'icon' => 'gallery',
            'keywords' => ['example'],
            'mode' => 'auto',
        ]);
    }

    public function my_add_template_to_posts()
    {
        $post_type_object = get_post_type_object('producten');
        $post_type_object->template = [
            ['core/paragraph'],
            ['acf/overgang'],
            ['core-embed/youtube'],
            ['acf/productafbeeldingen'],
            ['acf/ingredienten'],
            ['acf/verkooppuntvinden'],
            ['acf/faq']
        ];

        $post_type_object = get_post_type_object('recepten');
        $post_type_object->template = [
            ['acf/bereidingswijze'],
            ['acf/overgang'],
            ['core-embed/youtube'],
            ['acf/productafbeeldingen']
        ];


        //$post_type_object->template_lock = 'all';
    }

    public function render_products_block($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        $args = [
            'post_type' => 'producten',
            'posts_per_page' => -1,
            'orderby' => [
                'date' => 'DESC'
            ]
        ];

        $context['producten'] = Timber::get_posts($args);

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render($block['render_template'], $context);
    }

    public function render_faq($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        $args = [
            'post_type' => 'faq',
            'posts_per_page' => -1,
            'orderby' => [
                'date' => 'DESC'
            ]
        ];

        $context['faqs'] = Timber::get_posts($args);

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render($block['render_template'], $context);
    }

    public function render_block($block, $content = '', $is_preview = false)
    {
        global $post;
        $context = Timber::context();

        // Store block values.
        $context['post'] = $post;
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render($block['render_template'], $context);
    }

    public function render_subscribe_form($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        if ($_POST && !$is_preview) {
            $data = filter_input_array(INPUT_POST);
            $html = Timber::fetch('email/toegang_aanvraag.twig', ['data' => $data]);
            $to = get_option('admin_email');
            $headers[] = 'Content-Type: text/html; charset=UTF-8';
            $headers[] = 'Bcc: otmlogs+joannusmolen@onetomarket.nl';

            $context['mail_send'] = wp_mail($to, 'Aanvraag toegang', $html, $headers);
        }

        // Render the block.
        Timber::render('block/subscribe_form.twig', $context);
    }

    /**
     * Hide admin bar for non-admins
     */
    public function js_hide_admin_bar($show)
    {
        if (!current_user_can('administrator')) {
            return false;
        }
        return $show;
    }

    /**
     * Block wp-admin access for non-admins
     */
    public function ace_block_wp_admin()
    {
        if (is_admin() && !current_user_can('administrator') && !(defined('DOING_AJAX') && DOING_AJAX)) {
            wp_safe_redirect(home_url());
            exit;
        }
    }

    /**
     * Redirect users after login
     */
    public function js_login_redirect( $redirect_to, $request, $user ) {
        //is there a user to check?
        if (isset($user->roles) && is_array($user->roles)) {
            //check for subscribers
            if (in_array('subscriber', $user->roles)) {
                // redirect them to another URL, in this case, the homepage
                $redirect_to =  home_url();
            }
        }

        return $redirect_to;
    }


    /** This is where you can register custom post types. */
    public function register_post_types()
    {

    }

    /** This is where you can register custom taxonomies. */
    public function register_taxonomies()
    {

    }

    public function enqueue_script()
    {
        if (!is_admin()) {
            wp_enqueue_script('jquery', 'http://code.jquery.com/jquery-3.4.1.slim.min.js');
            wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', [], '1.14.3', true);
            wp_enqueue_script('fontawesome', 'https://kit.fontawesome.com/6794b11546.js', [], '1', false);
            wp_enqueue_script('bootstrap4', get_template_directory_uri() . '/assets/js/bootstrap.min.js', [], '4.3.1', true);
            wp_enqueue_script('gg', get_template_directory_uri() . '/assets/js/grid-gallery.min.js', ['jquery'], 0, true);
        }
    }

    public function enqueue_style()
    {
        if (!is_admin()) {
            wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', [], '4.3.1');
            wp_enqueue_style('joanusmolen', get_template_directory_uri() . '/assets/css/joannusmolen.min.css', [], '1.1');
            wp_enqueue_style('php-vars', get_template_directory_uri() . '/assets/css/style.php', [], '1.1');
        }
    }

    /** This is where you add some context
     *
     * @param string $context context['this'] Being the Twig's {{ this }}.
     */
    public function add_to_context($context)
    {
        $context['menu'] = new Timber\Menu('menu');
        $context['footer_menu'] = new Timber\Menu('footer');
        $context['footer_links'] = new Timber\Menu('footer links', ['depth' => 1]);
        $context['options'] = get_fields('options');

        if (function_exists('yoast_breadcrumb')) {
            $context['breadcrumbs'] = yoast_breadcrumb('<nav id="breadcrumbs" class="main-breadcrumbs">', '</nav>', false);
        }

        $context['site'] = $this;
        return $context;
    }

    public function theme_supports()
    {
        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', [
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ]);

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support('post-formats', [
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'audio',
        ]);

        add_theme_support('menus');
        add_theme_support('align-wide');
    }

    /** This is where you can add your own functions to twig.
     *
     * @param string $twig get extension.
     */
    public function add_to_twig($twig)
    {
        $twig->addExtension(new Twig_Extension_StringLoader());
        return $twig;
    }

}

new StarterSite();
