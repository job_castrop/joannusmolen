<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

use Timber\Post;

$args = array(
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => 'ASC',
    'cat' => ( get_queried_object() )->term_id
);

$context = Timber::get_context();
$context['posts'] = Timber::get_posts($args);

$templates = array( 'archive.twig', 'archive.twig', 'index.twig' );

Timber::render( $templates, $context );
