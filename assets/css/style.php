<?php
header("Content-type: text/css", true);
require(dirname(__FILE__) . '/../../../../../wp-config.php');
$frontpage = get_option('page_on_front');
$background_home = get_field('background_desktop', $frontpage)['url'] ?: "../images/header-home.png";
$background_home_mobile = get_field('background_mobile', $frontpage)['url'] ?: "../images/header-home-mobile.png";
?>

.header-back.header-home {
    background-image: url("<?=$background_home_mobile;?>");
}
/* MD */
@media (min-width: 768px) {
    .header-back.header-home {
    background-image: url("<?=$background_home;?>");
    }
}


