jQuery(document).on('click','.gg-element',function(){
  var selected=jQuery(this);
  var prev=jQuery(this).prev();
  var next=jQuery(this).next();
  jQuery('#gg-screen').show();
  var l=jQuery(".gg-element").length-1;
  var p=jQuery(".gg-element").index(selected);
  function buttons(){
    if (l > 1) {
      if (p == 0){
        return '<div class="gg-close gg-bt">&times</div><div class="gg-nxt gg-bt">&rarr;</div>';
      }
      else if (p == l) {
        return '<div class="gg-close gg-bt">&times</div><div class="gg-prev gg-bt">&larr;</div>';
      }
      else{
        return '<div class="gg-close gg-bt">&times</div><div class="gg-nxt gg-bt">&rarr;</div><div class="gg-prev gg-bt">&larr;</div>';
      }
    }
    else{
      return '<div class="gg-close gg-bt">&times</div>';
    }
  }
  var content=buttons();
  jQuery("#gg-screen").html('<div class="gg-image"></div>' + content);
  jQuery(".gg-image").html('<img src="'+ jQuery( this).attr('data-src') +'">');
  jQuery("body").css('overflow','hidden');
  jQuery(document).on('click','.gg-close',function(){
    jQuery("#gg-screen").hide();
    jQuery("body").css('overflow','auto');
  });
  jQuery("#gg-screen").on('click', function(e) {
    if (e.target == this){
      jQuery("#gg-screen").hide();
      jQuery("body").css('overflow','auto');
    }
  });
  jQuery(document).on('click','.gg-prev',function(){
    prev=selected.prev();
    var previmg='<img src="'+ prev.attr('data-src') +'">';
    jQuery(".gg-image").html(previmg);
    p=jQuery(".gg-element").index(prev);
    content=buttons();
    jQuery("#gg-screen").html('<div class="gg-image">'+ previmg + '</div>' + content);
    selected = prev;
  });
  jQuery(document).on('click','.gg-nxt',function(){
    next=selected.next();
    var nxtimg='<img src="'+ next.attr('data-src') +'">';
    jQuery(".gg-image").html(nxtimg);
    p=jQuery(".gg-element").index(next);
    content=buttons();
    jQuery("#gg-screen").html('<div class="gg-image">'+ nxtimg + '</div>' + content);
    selected=next;
  });
  jQuery(document).on('keydown',function(e) {
    if(e.keyCode == 37 && p>0) {
      prev=selected.prev();
      var previmg='<img src="'+ prev.attr('data-src') +'">';
      jQuery(".gg-image").html(previmg);
      p=jQuery(".gg-element").index(prev);
      content=buttons();
      jQuery("#gg-screen").html('<div class="gg-image">'+ previmg + '</div>' + content);
      selected=prev;
    }
    else if(e.keyCode == 39 && p < l) {
      next=selected.next();
      var nxtimg='<img src="'+ next.attr('data-src') +'">';
      jQuery(".gg-image").html(nxtimg);
      p=jQuery(".gg-element").index(next);
      content=buttons();
      jQuery("#gg-screen").html('<div class="gg-image">'+ nxtimg + '</div>' + content);
      selected=next;
    }
  });
});
