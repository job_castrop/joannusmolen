<?php
/*
Template Name: Productconfigurator
*/
use Timber\Post;

if (!is_user_logged_in()) {
    auth_redirect();
}
$context = Timber::get_context();

$timber_post = new Timber\Post();
$context['post'] = $timber_post;

foreach ([
             'productcategorie',
             'producttype',
             'recipe',
             'flavour',
             'variants',
             'content',
             'consumer_unit'
         ] as $posttype) {
    $obj = get_post_type_object($posttype);
    $context['conf_options'][$obj->labels->singular_name] = Timber::get_posts(['post_type' => $posttype]);
}

$templates = [$timber_post->slug . '.twig', 'page.twig'];

if ($_POST) {
    $data = filter_input_array(INPUT_POST);
    $html = Timber::fetch('email/offerte_aanvraag.twig', ['data' => $data]);
    $to = get_option('admin_email');
    $headers[] = 'Content-Type: text/html; charset=UTF-8';
    $headers[] = 'Bcc: otmlogs+joannusmolen@onetomarket.nl';

    wp_mail($to, 'Offerte aanvraag', $html, $headers);

    $templates = ['mail_send.twig'];
}

Timber::render($templates, $context);
