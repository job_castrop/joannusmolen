<?php
/*
Template Name: Contact
*/

acf_form_head();

$context = Timber::get_context();

$timber_post = new Timber\Post();
$context['post'] = $timber_post;
$templates = array( 'page-' . $timber_post->post_name . '.twig', 'page.twig' );

Timber::render( $templates, $context );
