<?php
/*
Template Name: Recepten
*/
use Timber\Post;

$context = Timber::get_context();

$timber_post = new Timber\Post();
$context['post'] = $timber_post;

$args = [
    'post_type' => 'recepten',
    'posts_per_page' => -1,
    'orderby' => [
        'date' => 'DESC'
    ]
];

$context['categories'] = Timber::get_terms('category', array('parent' => 0));
$context['subcategories'] = Timber::get_terms('category', array('parent' => $context['categories'][0]->id));
$context['recepten'] = Timber::get_posts($args);

$templates = ['page-' . $timber_post->slug . '.twig', 'page.twig'];

Timber::render($templates, $context);
